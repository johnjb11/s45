import {useState} from 'react'

import {Card, Button} from 'react-bootstrap'

export default function CourseCard ({courseProp}) {

	console.log(courseProp)

const {name, description, price} = courseProp

const [count, setCount] = useState(0)


//s46 activity
function enroll(){

	if(count === 30){
        alert('NO MORE SEATS.')
    } else {
	setCount(count + 1);
	console.log('Enrollees' + count)
}
}

	return (	
			<Card>
				<Card.Body>
					<Card.Title>{name}</Card.Title>
					<Card.Subtitle>Description</Card.Subtitle>
					<Card.Text>{description}</Card.Text>
									
					<Card.Subtitle>Price</Card.Subtitle>
					<Card.Text>{price}</Card.Text>
					<Card.Text>Enrollees: {count}</Card.Text>

					<Button variant = "primary" onClick ={enroll}>Enroll</Button>
				</Card.Body>
			</Card>	
		)
}



	
