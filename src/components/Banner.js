import {Row, Col, Button} from 'react-bootstrap'

//always start with these codes when creating a new component
export default function Banner (){
	return (
		<Row>
			<Col className = "p-5">
				<h1>Zuitt Coding Bootcamp</h1>
				<p>Opportunities for everyone, everywhere</p>
				<Button variant = "primary">Enroll Now</Button>
			</Col>
		</Row>

		)
}

/*let {name, age} = person

person = {
	name:
	age:
}*/